// Create a markdown link on a webpage [TITLE](URL)
// + add time to watch if it is a youtube video, or time to read if it is an article and
// the "Reading Time" chrome extension is installed.
javascript:
(function () {
  s = `[${document.querySelector("title").innerHTML
    }
  ](${window.location})`;
  elt = document.querySelector(".ytp-time-duration");
  if (elt) {
    s = `${s} ⏳ ${elt.innerHTML}`;
  } else {
    elt = document.querySelector("body > div:nth-child(1) > div");
    if (elt && elt.innerHTML.includes("hrs")) {
      timestr = elt.innerHTML.split(" ")[0];
      hours = parseInt(timestr.split(":")[0]);
      minutes = parseInt(timestr.split(":")[1]);
      if (hours == 0) {
        timestr = `${minutes}m`;
      } else {
        timestr = `${hours}h${minutes}m`;
      }
      s = `${s} ⏳ ${timestr}`;
    }
  }
  navigator.clipboard.writeText(s).then(() => { alert(s) });
}
)()

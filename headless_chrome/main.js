const puppeteer = require('puppeteer')

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const main = async () => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.goto('https://www.artstation.com/c2ba/connections')
  //await page.waitForNavigation();

  const followingCountSelector = "body > div.wrapper > div.wrapper-main > div:nth-child(5) > div > div.sub-nav > div > a.btn.btn-default.active"

  await Promise.race([
    page.waitForNavigation({ waitUntil: "networkidle0" }),
    page.waitForSelector(followingCountSelector)
  ]);

  const followingCountText = await page.evaluate((followingCountSelector) => {
    const elmt = document.querySelector(followingCountSelector)
    return elmt.innerHTML
  }, followingCountSelector)

  console.log(followingCountText)
  const followingCount = parseInt(/Following \(([^)]+)\)/.exec(followingCountText)[1]);

  console.log(`Number of following: ${followingCount}`)

  await Promise.race([
    page.waitForNavigation({ waitUntil: "networkidle0" }),
    page.waitForSelector("a.user-name")
  ]);

  const scrollFunc = () => {
    const elmts = document.querySelectorAll("a.user-name")
    window.scrollTo(0, document.body.scrollHeight);
    return elmts.length
  }

  let count = 0
  while (count < followingCount) {
    count = await page.evaluate(scrollFunc)
    await sleep(200)
  }

  // const test = await page.evaluate((followingCount) => {
  //   const elmts = document.querySelectorAll("a.user-name")
  //   //const intervalID = window.setInterval()
  //   while (elmts.length < followingCount) {
  //     window.scrollTo(0, document.body.scrollHeight);
  //   }
  //   return [...elmts].map(elmt => elmt.getAttribute("href"))
  // }, followingCount)

  //console.log(test)

  // const elmt = await page.$("a.user-name")

  // if (elmt) {
  //   console.log(elmt)
  // } else {
  //   console.log("b")
  // }

  await page.pdf({ path: 'page.pdf', format: 'A4' })
  await browser.close()
}

main()